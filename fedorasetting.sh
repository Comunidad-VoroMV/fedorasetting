#!/bin/bash
echo
 echo "************************************************************************"
 echo "**                                                                    **"
 echo "**                           FEDORA SETTING                           **"  
 echo "**                                                                    **"
 echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para configurar tu Fedora 36 Workstation."
sleep 2s
echo
echo ¡Vamos a ello!
sleep 3s
echo
echo "Empezemos configurando los repositorios de RPM Fusion y de flathub"
echo
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf update --refresh -y
sudo dnf groupupdate core -y
sudo dnf install -y rpmfusion-free-appstream-data rpmfusion-nonfree-appstream-data
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
echo
echo "Continuamos instalando algunos paquetes básicos"
echo
sudo dnf install -y gstreamer1-libav gstreamer1-plugins-bad-free-extras gstreamer1-plugins-bad-freeworld gstreamer1-plugins-good-extras gstreamer1-plugins-ugly unrar p7zip p7zip-plugins gstreamer1-plugin-openh264 mozilla-openh264 openh264 webp-pixbuf-loader gstreamer1-plugins-bad-free-fluidsynth gstreamer1-plugins-bad-free-wildmidi gstreamer1-svt-av1 libopenraw-pixbuf-loader dav1d file-roller
echo


    read -p "¿Quieres instalar los drivers privativos de nvidia (s/n)?" sn
    case $sn in
        [Ss]* )  sudo dnf install -y akmod-nvidia ; sudo dnf install -y xorg-x11-drv-nvidia-cuda ; sudo dnf update -y;;
        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

echo

    read -p "¿Quieres instalar los drivers de intel para activar la aceleración por hardware (s/n)?" sn
    case $sn in
        [Ss]* )  sudo dnf install -y libva-intel-driver intel-media-driver libva libva-utils;;
        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

echo

    read -p "¿Quieres instalar algunos paquetes para renderizar vídeo (s/n)?" sn
    case $sn in
        [Ss]* )  sudo dnf install -y x264 h264enc x265 svt-av1 rav1e;;
        [Nn]* ) echo;;
        * ) echo "Por favor, pulsa s o n.";;
    esac

echo

echo "Por último instalaremos 2 herramientas con las que poder administrar la configuración de SELinux y el firewall de forma gráfica."
echo
sudo dnf install -y policycoreutils-gui firewall-config
echo
echo "Todo listo, te recomiendo reiniciar el sistema para que todos los cambios se apliquen correctamente el sistema."
echo

    read -p "¿Quieres reiniciar el sistema ahora (s/n)?" sn
    case $sn in
        [Ss]* )  sudo reboot;;
        [Nn]* ) exit;;
        * ) echo "Por favor, pulsa s o n.";;
    esac
done